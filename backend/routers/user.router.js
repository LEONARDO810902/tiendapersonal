const { Router } = require('express');
const routeruser = Router();
const { check } = require("express-validator");


// nuestros controladores 
var ControllerUser = require('../controllers/ControllerUsers')

//rutas
routeruser.get('/buscar/:id', ControllerUser.buscarUser);
routeruser.get('/buscarall/:idb?', ControllerUser.listarUserAllData);
routeruser.post('/crear', ControllerUser.saveUsers);
routeruser.delete('/users/:id',ControllerUser.deleteUsers);
routeruser.put('/updateusers/:id',ControllerUser.updateUsers);
routeruser.post('/crearUser', ControllerUser.crearUsuario);

module.exports = routeruser;