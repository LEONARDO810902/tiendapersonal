const { Router } = require('express');
const routerProducto = Router();

var ControllerProducto = require('../controllers/ControllerProducto')

routerProducto.post('/crear', ControllerProducto.saveProducto )
routerProducto.get('/buscar/:id', ControllerProducto.buscarProducto )
routerProducto.get('/listar/:idb?', ControllerProducto.listarProductos);
routerProducto.put('/update/:id?', ControllerProducto.updateProductos);
routerProducto.delete('/delete/:id?', ControllerProducto.deleteProductos);

module.exports = routerProducto;