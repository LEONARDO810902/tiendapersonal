const { Router } = require('express');
const router = Router();


// nuestros controladores 
const routeruser = require('./user.router')
const routerProducto = require('./producto.router')
const routerAuth = require('./auth');

//rutas
router.use("/users", routeruser)
router.use("/productos", routerProducto)
router.use("/api/auth", routerAuth)



module.exports = router;