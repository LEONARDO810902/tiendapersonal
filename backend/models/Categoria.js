const mongoose = require ('mongoose');
const {Schema } = mongoose;

const CategoriaSchema = new Schema ({
    nombre: {type: String, requiere: true},
    estado: {type: Boolean},
    creacion: { type: Date, default: Date.now() },
});

const Categoria = mongoose.model('categoria', CategoriaSchema);

modeule.exports = Categoria; 