const mongoose = require ('mongoose');
const { Schema } = mongoose;

const ProductoSchema = new Schema ({
    codigo: {type: String, require: true},
    nombre: {type: String, requiere: true},
    categoria: {type: String, requiere: true},
    descripcion: {type: String, requiere: true},
    valor: {type: Number, requiere: true},
    iva: {type: Number},
    estado: {type: Boolean},
    creacion: { type: Date, default: Date.now() },
});

const Productos =  mongoose.model('productos', ProductoSchema);

module.exports = Productos;