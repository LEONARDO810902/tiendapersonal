const mongoose = require ('mongoose');
const { Schema } = mongoose;

const PedidosSchema = new Schema ({
    codigo: {type: String, require: true},
    nombre: {type: String, requiere: true},
    categoria: {type: String, requiere: true},
    cantidad: {type: Number, require: true}, 
    creacion: { type: Date, default: Date.now() },
});

const Pedidos =  mongoose.model('pedidos', PedidosSchema);

module.exports = Pedidos;