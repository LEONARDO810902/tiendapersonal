const mongoose = require('mongoose');

const Productos = require ('../models/Productos');

function saveProducto(req, res) {
    var myProducto = new Productos(req.body);
    myProducto.save((err, result) => {
      res.status(200).send({ message: result });
    });
  }


  function buscarProducto(req, res) {
    var idproducto = req.params.id;
    Productos.findById(idproducto).exec((err, result) => {
      if (err) {
        res
          .status(500)
          .send({ message: "Error al momento de ejecutar la solicitud" });
      } else {
        if (!result) {
          res
            .status(404)
            .send({ message: "El registro a buscar no se encuentra disponible" });
        } else {
          res.status(200).send({ result });
        }
      }
    });
  }


  function listarProductos(req, res) {
    var idProductos = req.params.idb;
    if (!idProductos) {
      var result = Productos.find({}).sort("categoria");
    } else {
      var result = Productos.find({ _id: idProductos }).sort("categoria");
    }
    result.exec(function (err, result) {
      if (err) {
        res
          .status(500)
          .send({ message: "Error al momento de ejecutar la solicitud" });
      } else {
        if (!result) {
          res
            .status(404)
            .send({ message: "El registro a buscar no se encuentra disponible" });
        } else {
          res.status(200).send({ result });
        }
      }
    });
  }
  
  function updateProductos(req, res) {
    var id = mongoose.Types.ObjectId(req.params.id);
    console.log(id)
    Productos.findOneAndUpdate( { _id: id },req.body, { new: true },
      function (err, Productos) {
        if (err) res.send(err);
        console.log(Productos);
        res.json(Productos);
      }
    );
  }
  
  function deleteProductos(req, res) {
    var idProductos = req.params.id;
    Productos.findByIdAndRemove(idProductos, function (err, Productos) {
      if (err) {
        return res.json(500, {
          message: "No hemos encontrado el usuario",
        });
      }
      return res.json(Productos);
    });
  }



  module.exports = {
    saveProducto,
    buscarProducto,
    listarProductos,
    updateProductos,
    deleteProductos,
  }