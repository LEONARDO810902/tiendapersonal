const mongoose = require ('mongoose');
const { validationResult } = require("express-validator");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Usuario = require("../models/Usuario");

function saveUsers(req, res) {
  var myUsers = new Usuario(req.body);
  myUsers.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

crearUsuario = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { email, password } = req.body;

  try {
    //Revisar que el usuario registrado sea único
    let usuario = await Usuario.findOne({ email });

    if (usuario) {
      return res.status(400).json({ msg: "El usuario ya existe" });
    }

    //crear el nuevo usuario
    usuario = new Usuario(req.body);

    usuario.password = await bcryptjs.hash(password, 10);

    //Guardar usuario en la bd
    await usuario.save();

    //Firmar el JWT
    const payload = {
      usuario: { id: usuario.id },
    };

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 3600, //1 hora
      },
      (error, token) => {
        if (error) throw error;

        //Mensaje de confirmación
        res.json({ token });
      }
    );
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};


function buscarUser(req, res) {
  var idusers = req.params.id;
  Usuario.findById(idusers).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}

function listarUserAllData(req, res) {
  var idUser = req.params.idb;
  if (!idUser) {
    var result = Usuario.find({}).sort("nombre");
  } else {
    var result = Usuario.find({ _id: idUser }).sort("nombre");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}

function updateUsers(req, res) {
    console.log('estoy aqui')
  var id = mongoose.Types.ObjectId(req.params.id);
  console.log(id)
  Usuario.findOneAndUpdate( { _id: id },req.body, { new: true },
    function (err, Users) {
        console.log('estoy aqui')
      if (err) res.send(err);
      console.log(Users);
      res.json(Users);
    }
  );
}

function deleteUsers(req, res) {
  var idusers = req.params.id;
  Usuario.findByIdAndRemove(idusers, function (err, users) {
    if (err) {
      return res.json(500, {
        message: "No hemos encontrado el usuario",
      });
    }
    return res.json(users);
  });
}


module.exports = {
  saveUsers,
  buscarUser,
  listarUserAllData,
  updateUsers,
  deleteUsers,
  crearUsuario
};
