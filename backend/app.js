const express = require ('express');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require("method-override");


app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))


// configurar las cabeceras y cors 

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, x-auth-token, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
    next();
});



//Routers
app.use(require('./routers/routers'));

module.exports = app;

