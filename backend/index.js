var app = require('./app')

const express = require ('express');
const morgan = require('morgan');
const path = require('path')
const cors = require("cors");

const conectarDB = require('./db/conn')

conectarDB();
//var mongoose = require('./db/conn')

//habilitar cors
app.use(cors());

// Settings
app.set('port', process.env.PORT || 4000); // Para configurar el puerto en produccion 


// Middlewares
app.use(morgan('dev'));
app.use(express.json());


//Static files
//app.use(express.static(path.join(__dirname + '/public')))
//console.log(path.join(__dirname + '/public'))



app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});