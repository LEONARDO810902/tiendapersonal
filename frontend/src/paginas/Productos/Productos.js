import React from "react";
import { Link } from "react-router-dom";

import Narbar from "../../componentes/Narvar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";

const Productos = () => {
    
        const ListaProductos = async () =>{
            const response = await APIInvoke.invokeGET('/productos/listar')
            console.log(response)
        }

        window.addEventListener("load", async() =>{
            await ListaProductos();
        })

    

    return (
    <div className="wrapper">
      <Narbar></Narbar>
      <SidebarContainer></SidebarContainer>

      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Listado de Productos</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <Link to={"#"}>Home</Link>
                  </li>
                  <li className="breadcrumb-item active">Blank Page</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section className="content my-4">
            <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <table id="tableProductos" className="table">
                    <thead>
                    <tr>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody id="tableBodyProductos">
                        
                    </tbody>
                </table>
            </div>
        </section>
      </div>
    </div>
  );

};

export default Productos;
