import React from "react";

import Narbar from "../componentes/Narvar";
import SidebarContainer from "../componentes/SidebarContainer";

const Home = () => {
    return (
        <div className="wrapper">
            <Narbar></Narbar>
            <SidebarContainer></SidebarContainer>
        </div>
    );
}

export default Home;