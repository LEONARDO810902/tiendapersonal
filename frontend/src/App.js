import React, { Fragment }from 'react';
import { BrowserRouter as Router, Route, Routes  } from 'react-router-dom';
import Login from './paginas/auth/Login';
import CrearCuenta from './paginas/auth/CrearCuenta';
import Home from './paginas/Home';
import Productos from './paginas/Productos/Productos';


function App() {

  return (
   <Fragment>
    <Router>
      <Routes>
        <Route path="/login" exact element={<Login/>} />
        <Route path="/" exact element={<Home />} />
        <Route path="/crear-cuenta" exact element={<CrearCuenta />} />

        <Route path="/listarproductos" exact element={<Productos />} />
      </Routes>
    </Router>
   </Fragment>
  );
}

export default App;
